module gitlab.com/bichon-project/bichon

go 1.12

require (
	github.com/casimir/xdg-go v0.0.0-20160329195404-372ccc2180da
	github.com/gdamore/tcell v1.3.0
	github.com/go-ini/ini v1.50.0
	github.com/golang/glog v0.0.0-20160126235308-23def4e6c14b
	github.com/pgavlin/femto v0.0.0-20191028012355-31a9964a50b5
	github.com/rivo/tview v0.0.0-20191018125527-685bf6da76c2
	github.com/spf13/pflag v1.0.5
	github.com/xanzy/go-gitlab v0.22.0
	github.com/zalando/go-keyring v0.0.0-20190913082157-62750a1ff80d
	gopkg.in/src-d/go-git.v4 v4.13.1
)
