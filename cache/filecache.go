// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package cache

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"gitlab.com/bichon-project/bichon/model"
)

type fileCache struct {
	Directory string
}

func (cache *fileCache) repoDir(repo *model.Repo) string {
	project := []string{cache.Directory, repo.Server}
	bits := strings.Split(repo.Project, "/")
	project = append(project, bits...)
	return filepath.Join(project...)
}

func (cache *fileCache) repoFile(repo *model.Repo, mreq uint) string {
	dir := cache.repoDir(repo)

	return filepath.Join(dir, fmt.Sprintf("%d.json", mreq))
}

func NewFileCache(dir string) Cache {
	return &fileCache{
		Directory: dir,
	}
}

func (cache *fileCache) AddRepo(repo *model.Repo) error {
	dir := cache.repoDir(repo)
	return os.MkdirAll(dir, os.ModePerm)
}

func (cache *fileCache) RemoveRepo(repo *model.Repo) error {
	dir := cache.repoDir(repo)
	return os.RemoveAll(dir)
}

func (cache *fileCache) ListMergeRequests(repo *model.Repo) ([]uint, error) {
	dir := cache.repoDir(repo)
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		return []uint{}, nil
	}
	var ids []uint
	for _, file := range files {
		name := file.Name()
		if !strings.HasSuffix(name, ".json") {
			continue
		}

		name = name[0 : len(name)-5]

		id, err := strconv.ParseUint(name, 10, 64)
		if err != nil {
			continue
		}
		ids = append(ids, uint(id))
	}
	return ids, nil
}

func (cache *fileCache) SaveMergeRequest(mreq *model.MergeReq) error {
	file := cache.repoFile(&mreq.Repo, mreq.ID)
	data, err := mreq.ToJSON()
	if err != nil {
		return err
	}
	return ioutil.WriteFile(file, data, 0600)
}

func (cache *fileCache) LoadMergeRequest(repo *model.Repo, id uint) (*model.MergeReq, error) {
	file := cache.repoFile(repo, id)
	data, err := ioutil.ReadFile(file)
	if err != nil {
		os.Remove(file)
		return nil, err
	}

	mreq, err := model.NewMergeReqFromJSON(data)
	if err != nil {
		os.Remove(file)
		return nil, err
	}

	if mreq.ID != id {
		os.Remove(file)
		return nil, err
	}

	mreq.Repo = *repo

	if mreq.Metadata.Status == model.STATUS_NEW || mreq.Metadata.Status == model.STATUS_UPDATED {
		mreq.Metadata.Status = model.STATUS_OLD
	}

	return mreq, nil
}
