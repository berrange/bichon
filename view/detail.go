// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package view

import (
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/gdamore/tcell"
	"github.com/golang/glog"
	"github.com/pgavlin/femto"
	"github.com/rivo/tview"

	"gitlab.com/bichon-project/bichon/model"
)

type DetailPageListener interface {
	DetailPageQuit()
	DetailPageRefreshMergeRequest(mreq *model.MergeReq)
	DetailPageAddMergeReqComment(mreq *model.MergeReq, text string, context *model.CommentContext)
	DetailPageAddMergeReqReply(mreq *model.MergeReq, thread, text string)
	DetailPageLoadMergeReqCommitDiffs(mreq *model.MergeReq, commit *model.Commit)
	DetailPageAcceptMergeReq(mreq *model.MergeReq)
	DetailPageApproveMergeReq(mreq *model.MergeReq)
	DetailPageUnapproveMergeReq(mreq *model.MergeReq)
}

type DetailPage struct {
	*tview.Flex

	Application    *tview.Application
	Listener       DetailPageListener
	Patches        *tview.Table
	Header         *tview.Frame
	Patch          *model.Commit
	Content        *tview.TextView
	ContentLine    int
	ContentLen     int
	ContentRegions []PatchRegion
	CommentText    *femto.View
	CommentForm    *tview.Form
	CommentFrame   *tview.Frame
	CommentEdit    bool
	CommentRegion  *PatchRegion
	CommentOffset  uint

	Layout *tview.Flex

	MergeReq *model.MergeReq
}

func NewDetailPage(app *tview.Application, listener DetailPageListener) *DetailPage {
	patches := tview.NewTable().
		SetSelectable(true, false).
		SetSelectedStyle(
			GetStyleColor(ELEMENT_MREQS_ACTIVE_TEXT),
			GetStyleColor(ELEMENT_MREQS_ACTIVE_FILL),
			GetStyleAttrMask(ELEMENT_MREQS_ACTIVE_ATTR)).
		SetFixed(1, 0)

	header := tview.NewFrame(patches).
		SetBorders(0, 0, 0, 0, 0, 0).
		AddText(fmt.Sprintf("[%s:%s:%s]",
			GetStyleColorName(ELEMENT_HEADER_TEXT),
			GetStyleColorName(ELEMENT_HEADER_FILL),
			GetStyleColorName(ELEMENT_HEADER_ATTR))+
			strings.Repeat(" ", 500),
			false, tview.AlignLeft, tcell.ColorWhite)

	content := tview.NewTextView().
		SetDynamicColors(true).
		SetRegions(true)

	commentBuf := femto.NewBufferFromString("", "comment.txt")
	commentBuf.Settings["softwrap"] = true
	commentText := femto.NewView(commentBuf)

	commentForm := tview.NewForm()
	commentFlex := tview.NewFlex().
		SetDirection(tview.FlexRow).
		AddItem(commentText, 0, 1, false).
		AddItem(commentForm, 3, 1, false)
	commentFrame := tview.NewFrame(commentFlex)

	commentFrame.SetBorder(true).SetTitle("Add comment")

	layout := tview.NewFlex().
		SetDirection(tview.FlexRow).
		AddItem(header, 7, 1, false).
		AddItem(content, 0, 1, false)

	page := &DetailPage{
		Flex: layout,

		Application:  app,
		Listener:     listener,
		Patches:      patches,
		Header:       header,
		Content:      content,
		CommentFrame: commentFrame,
		CommentText:  commentText,
		CommentForm:  commentForm,
		Layout:       layout,
	}

	commentForm.AddButton("Save", page.saveComment)
	commentForm.AddButton("Cancel", page.cancelComment)

	patches.SetSelectionChangedFunc(page.switchPatch)

	return page
}

func (page *DetailPage) GetName() string {
	return "detail"
}

func (page *DetailPage) GetKeyShortcuts() string {
	return "q:Index r:Refresh c:Comment a:Approve A:Unapprove m:Merge ?:Help"
}

func (page *DetailPage) buildMergeReqRow(mreq *model.MergeReq) [7]*tview.TableCell {
	nversions := len(mreq.Versions)
	version := mreq.Versions[len(mreq.Versions)-1]
	npatches := len(version.Patches)

	return [7]*tview.TableCell{
		&tview.TableCell{
			Text:            fmt.Sprintf("%5s", fmt.Sprintf("#%d", mreq.ID)),
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignRight,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            fmt.Sprintf("%20s", TrimEllipsisFront(mreq.Repo.Project, 20)),
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignRight,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            fmt.Sprintf(" %-8s", mreq.Age()),
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignLeft,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            fmt.Sprintf("%20s", TrimEllipsisFront(mreq.Submitter.RealName, 20)),
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignLeft,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            fmt.Sprintf("%4s", fmt.Sprintf("v%d", nversions)),
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignLeft,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            fmt.Sprintf("%7s", fmt.Sprintf("0/%d", npatches)),
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignLeft,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            mreq.Title,
			Expansion:       1,
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignLeft,
			NotSelectable:   false,
		},
	}
}

func (page *DetailPage) buildMergeReqPatchRow(mreq *model.MergeReq, series *model.Series, idx int) [7]*tview.TableCell {
	patch := &series.Patches[idx]
	npatches := len(series.Patches)

	var marker string
	if idx == (len(series.Patches) - 1) {
		marker = "└─>  "
	} else {
		marker = "├─>  "
	}
	return [7]*tview.TableCell{
		&tview.TableCell{
			Text:            "",
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignRight,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            marker,
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignRight,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            fmt.Sprintf(" %-8s", patch.Age()),
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignLeft,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            fmt.Sprintf("%20s", TrimEllipsisFront(patch.Author.Name, 20)),
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignLeft,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            "",
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignLeft,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            fmt.Sprintf("%7s", fmt.Sprintf("%d/%d", idx+1, npatches)),
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignLeft,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            patch.Title,
			Color:           GetStyleColor(ELEMENT_MREQS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_MREQS_INACTIVE_FILL),
			Align:           tview.AlignLeft,
			NotSelectable:   false,
		},
	}
}

func (page *DetailPage) refreshMain(app *tview.Application, mreq *model.MergeReq) {
	glog.Infof("Refreshing detail page main")
	page.MergeReq = mreq
	page.updatePatches()
	page.updateContent()
}

func (page *DetailPage) Refresh(app *tview.Application, mreq *model.MergeReq) {
	glog.Infof("Refreshing detail page queue")
	app.QueueUpdateDraw(func() {
		page.refreshMain(app, mreq)
	})
}

func (page *DetailPage) Activate() {
	page.Layout.RemoveItem(page.CommentFrame)
	page.Application.SetFocus(page.Content)
	page.Patches.Select(0, 0)
}

func (page *DetailPage) saveComment() {
	text := page.CommentText.Buf.String()
	if text != "" {
		if page.CommentRegion != nil {
			if page.CommentRegion.Thread == "" {
				context := page.getCommitContext(page.CommentRegion, page.CommentOffset)
				page.Listener.DetailPageAddMergeReqComment(page.MergeReq, text, context)
			} else {
				page.Listener.DetailPageAddMergeReqReply(page.MergeReq, page.CommentRegion.Thread, text)
			}
		} else {
			page.Listener.DetailPageAddMergeReqComment(page.MergeReq, text, nil)
		}
	}

	page.cancelComment()
}

func (page *DetailPage) cancelComment() {
	page.Layout.RemoveItem(page.CommentFrame)
	page.Application.SetFocus(page.Content)
	page.CommentEdit = false
	page.CommentRegion = nil
}

func (page *DetailPage) moveCursor(delta int) {
	page.ContentLine += delta
	if page.ContentLine < 1 {
		page.ContentLine = 1
	} else if page.ContentLine > page.ContentLen {
		page.ContentLine = page.ContentLen
	}
	page.Content.Highlight(fmt.Sprintf("l%d", page.ContentLine))
	page.Content.ScrollToHighlight()
}

func (page *DetailPage) findRegion() (*PatchRegion, uint) {
	ids := page.Content.GetHighlights()

	if len(ids) != 1 {
		glog.Infof("No regions highlighted")
		return nil, 0
	}

	if !strings.HasPrefix(ids[0], "l") {
		glog.Infof("Unexpected region id '%s'", ids[0])
		return nil, 0
	}

	id, err := strconv.Atoi(ids[0][1:])
	if err != nil {
		glog.Infof("Malformed region id '%s'", ids[0][1:])
		return nil, 0
	}

	for idx, _ := range page.ContentRegions {
		region := &page.ContentRegions[idx]
		if id >= region.IDStart && id <= region.IDEnd {
			glog.Infof("Found %d in region (%d-%d) type %d file old %s#%d new %s#%d",
				id, region.IDStart, region.IDEnd, region.Type,
				region.OldFile, region.OldLine, region.NewFile, region.NewLine)
			return region, uint(id - region.IDStart)
		}
	}

	glog.Infof("No matching region ID")
	return nil, 0
}

func (page *DetailPage) getCommitContext(region *PatchRegion, offset uint) *model.CommentContext {
	ver := page.MergeReq.Versions[len(page.MergeReq.Versions)-1]
	parent := ver.BaseHash
	for _, patch := range ver.Patches {
		if patch.Hash == page.Patch.Hash {
			break
		}
		parent = patch.Hash
	}

	/* Note that even if OldLine or NewLine are zero
	 * (ie removed or added lines), we must always pass
	 * OldFile and NewFile names. If either filename is
	 * missing then the github web UI won't display the comment
	 */
	oldline := region.OldLine
	if oldline != 0 {
		oldline += offset
	}
	newline := region.NewLine
	if newline != 0 {
		newline += offset
	}

	return &model.CommentContext{
		BaseHash:  parent,
		StartHash: parent,
		HeadHash:  page.Patch.Hash,
		OldFile:   region.OldFile,
		OldLine:   oldline,
		NewFile:   region.NewFile,
		NewLine:   newline,
	}
}

func (page *DetailPage) HandleInput(event *tcell.EventKey) *tcell.EventKey {
	if page.CommentText.HasFocus() {
		switch event.Key() {
		case tcell.KeyTab:
			page.CommentForm.SetFocus(0)
			page.Application.SetFocus(page.CommentForm)
		case tcell.KeyBacktab:
			page.Application.SetFocus(page.Content)
		default:
			return event
		}
		return nil
	}

	if page.CommentForm.HasFocus() {
		if page.CommentForm.GetButton(1).HasFocus() &&
			event.Key() == tcell.KeyTab {
			page.Application.SetFocus(page.Content)
			return nil
		}
		if page.CommentForm.GetButton(0).HasFocus() &&
			event.Key() == tcell.KeyBacktab {
			page.Application.SetFocus(page.CommentText)
			return nil
		}
		return event

	}

	switch event.Key() {
	case tcell.KeyRune:
		switch event.Rune() {
		case 'q':
			page.Listener.DetailPageQuit()
		case 'c':
			glog.Infof("Maybe activating comment")
			showform := false
			page.CommentRegion = nil
			page.CommentOffset = 0
			if page.Patch != nil {
				region, offset := page.findRegion()
				if region != nil {
					page.CommentRegion = region
					page.CommentOffset = offset
					showform = true
				}
			} else {
				showform = true
			}

			if showform {
				buf := femto.NewBufferFromString("", "comment.txt")
				buf.Settings["softwrap"] = true
				page.CommentText.OpenBuffer(buf)
				page.CommentText.ToggleRuler()
				page.Layout.AddItem(page.CommentFrame, 20, 0, true)
				page.Application.SetFocus(page.CommentText)
				page.CommentEdit = true
			}
		case 'm':
			if page.MergeReq != nil {
				page.Listener.DetailPageAcceptMergeReq(page.MergeReq)
			}
		case 'a':
			if page.MergeReq != nil {
				page.Listener.DetailPageApproveMergeReq(page.MergeReq)
			}
		case 'A':
			if page.MergeReq != nil {
				page.Listener.DetailPageUnapproveMergeReq(page.MergeReq)
			}
		case 'r':
			if page.MergeReq != nil {
				page.Listener.DetailPageRefreshMergeRequest(page.MergeReq)
			}
		default:
			return event
		}

	case tcell.KeyTab:
		if page.CommentEdit {
			page.Application.SetFocus(page.CommentText)
		}
	case tcell.KeyBacktab:
		if page.CommentEdit {
			page.CommentForm.SetFocus(1)
			page.Application.SetFocus(page.CommentForm)
		}
	case tcell.KeyLeft:
		if !page.CommentEdit {
			row, col := page.Patches.GetSelection()
			if row > 0 {
				page.Patches.Select(row-1, col)
			}
		}

	case tcell.KeyRight:
		if !page.CommentEdit {
			row, col := page.Patches.GetSelection()
			if row < (page.Patches.GetRowCount() - 1) {
				page.Patches.Select(row+1, col)
			}
		}

	case tcell.KeyPgUp:
		if page.ContentLen != 0 {
			page.moveCursor(-10)
		} else {
			return event
		}
	case tcell.KeyPgDn:
		if page.ContentLen != 0 {
			page.moveCursor(10)
		} else {
			return event
		}
	case tcell.KeyUp:
		if page.ContentLen != 0 {
			page.moveCursor(-1)
		} else {
			return event
		}
	case tcell.KeyDown:
		if page.ContentLen != 0 {
			page.moveCursor(1)
		} else {
			return event
		}
	default:
		return event
	}

	return nil
}

func (page *DetailPage) updatePatches() {
	page.Patches.Clear()

	mreq := page.MergeReq
	if mreq == nil {
		return
	}

	cells := make([][7]*tview.TableCell, 0)

	cells = append(cells, page.buildMergeReqRow(mreq))

	ver := &mreq.Versions[len(mreq.Versions)-1]
	for idx, _ := range ver.Patches {
		cells = append(cells, page.buildMergeReqPatchRow(mreq, ver, idx))
	}

	for idx, row := range cells {
		page.Patches.SetCell(idx, 0, row[0])
		page.Patches.SetCell(idx, 1, row[1])
		page.Patches.SetCell(idx, 2, row[2])
		page.Patches.SetCell(idx, 3, row[3])
		page.Patches.SetCell(idx, 4, row[4])
		page.Patches.SetCell(idx, 5, row[5])
		page.Patches.SetCell(idx, 6, row[6])
	}
	page.Patches.SetOffset(0, 0)
}

func (page *DetailPage) buildRegions(data string) string {
	lines := strings.Split(data, "\n")
	for idx, _ := range lines {
		lines[idx] = fmt.Sprintf("[\"n%d\"]", idx) + lines[idx] + "[\"\"]"
	}
	return strings.Join(lines, "\n")
}

func (page *DetailPage) formatCoverLetter() string {
	colorHeader :=
		GetStyleMarker(
			ELEMENT_SUMMARY_HEADER_TEXT,
			ELEMENT_SUMMARY_HEADER_FILL,
			ELEMENT_SUMMARY_HEADER_ATTR)
	colorTitle :=
		GetStyleMarker(
			ELEMENT_SUMMARY_TITLE_TEXT,
			ELEMENT_SUMMARY_TITLE_FILL,
			ELEMENT_SUMMARY_TITLE_ATTR)
	colorDate :=
		GetStyleMarker(
			ELEMENT_SUMMARY_DATE_TEXT,
			ELEMENT_SUMMARY_DATE_FILL,
			ELEMENT_SUMMARY_DATE_ATTR)

	mreq := page.MergeReq
	body := colorHeader + "Merge Request: " + "[-:-:-]" +
		colorTitle + tview.Escape(mreq.Title) + "[-:-:-]" +
		colorHeader + " on " + "[-:-:-]" +
		colorDate + mreq.CreatedAt.Format(time.RFC1123) + "[-:-:-]" +
		"\n\n" +
		tview.Escape(mreq.Description) + "\n\n\n"

	region := 0
	lines, _ := FormatThreads(mreq.Threads, true, &region, "")

	body += strings.Join(lines, "\n") + "\n"

	return body
}

func (page *DetailPage) formatPatch(num int) (*model.Commit, []string, []PatchRegion) {
	ver := &page.MergeReq.Versions[len(page.MergeReq.Versions)-1]
	if num >= len(ver.Patches) {
		return nil, []string{
			fmt.Sprintf("Commit %d does exist in this version", num),
		}, []PatchRegion{}
	}

	patch := &ver.Patches[num]
	if patch.Metadata.Partial {
		page.Listener.DetailPageLoadMergeReqCommitDiffs(page.MergeReq, patch)
	}

	lines, regions, err := FormatCommit(patch, page.MergeReq.Threads)
	if err != nil {
		return nil, []string{
			fmt.Sprintf("Unable to format commit %s", err),
		}, []PatchRegion{}
	}
	return patch, lines, regions
}

func (page *DetailPage) switchPatch(row, col int) {
	if page.MergeReq == nil {
		page.Content.SetText("")
		return
	}

	glog.Infof("Re-rendering patches %d", row)
	if row == 0 {
		page.Patch = nil
		page.ContentLen = 0
		page.ContentRegions = []PatchRegion{}
		page.ContentLine = 0
		page.Content.SetText(page.formatCoverLetter())
		page.Content.ScrollToBeginning()
	} else {
		patch, lines, regions := page.formatPatch(row - 1)
		page.Patch = patch
		page.ContentLen = len(lines)
		page.ContentRegions = regions
		page.ContentLine = 1
		page.Content.SetText(strings.Join(lines, "\n") + "\n")
		page.moveCursor(0)
	}
}

func (page *DetailPage) updateContent() {
	row, col := page.Patches.GetSelection()
	page.switchPatch(row, col)
}
