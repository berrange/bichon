// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package snake

import (
	"fmt"
	"math/rand"
	"time"

	"github.com/gdamore/tcell"
	"github.com/golang/glog"
	"github.com/rivo/tview"
)

const (
	RUNE_RUNNING  = '🐍'
	RUNE_GAMEOVER = '🐶'
)

var (
	RUNE_FRUIT = []rune{
		'🍉', '🍌', '🍇', '🍈', '🍊', '🍋', '🍌', '🍍', '🍎', '🍐', '🍑', '🍒', '🍓', '🥝', '🍅', '🥥', '🥑',
	}
)

type Direction int

const (
	DIR_NONE Direction = iota
	DIR_UP
	DIR_DOWN
	DIR_LEFT
	DIR_RIGHT
)

type Cell struct {
	X    int
	Y    int
	Char rune
}

type Snake struct {
	Body []*Cell
}

type Board struct {
	Width  int
	Height int
	Data   []rune
}

type Display interface {
	UpdateBoard(board *Board)
}

type Engine struct {
	Display    Display
	Width      int
	Height     int
	FPS        int
	Growth     int
	NextGrowth int
	Speed      int
	NextSpeed  int
	Direction  Direction
	Score      int
	Snake      *Snake
	Fruit      *Cell
	Refresh    *time.Ticker
	Quit       chan bool
	Move       chan Direction
}

type Listener interface {
	SnakeGameFinished()
}

type Game struct {
	*tview.Box

	Listener Listener
	App      *tview.Application
	Board    *Board
	Engine   *Engine
}

func NewGame(app *tview.Application, listener Listener) *Game {
	return &Game{
		Box:      tview.NewBox(),
		Listener: listener,
		App:      app,
	}
}

func NewEngine(display Display, width, height, fps int) *Engine {
	engine := &Engine{
		Display:    display,
		Width:      width,
		Height:     height,
		FPS:        fps,
		Growth:     fps * 4,
		NextGrowth: fps * 4,
		Speed:      fps * 8,
		NextSpeed:  fps * 8,
		Direction:  DIR_NONE,
		Snake:      NewSnake(width, height),
		Quit:       make(chan bool),
		Move:       make(chan Direction),
	}
	for engine.Fruit == nil || engine.Fruit.Hit(engine.Snake.Body[0]) {
		engine.Fruit = NewFruit(width, height, engine.Snake)
	}

	return engine
}

func NewSnake(width, height int) *Snake {
	return &Snake{
		Body: []*Cell{
			NewCellRand(width, height, RUNE_RUNNING),
		},
	}
}
func NewFruit(width, height int, snake *Snake) *Cell {
	i := rand.Intn(len(RUNE_FRUIT))
	char := RUNE_FRUIT[i]

	for {
		fruit := NewCellRand(width, height, char)
		for _, cell := range snake.Body {
			if !cell.Hit(fruit) {
				return fruit
			}
		}
	}

}

func NewCellRand(width, height int, char rune) *Cell {
	return &Cell{
		X:    rand.Intn(width),
		Y:    rand.Intn(height),
		Char: char,
	}
}

func NewCellJoined(width, height int, cell *Cell, dir Direction) *Cell {
	x := cell.X
	y := cell.Y
	switch dir {
	case DIR_NONE:
	case DIR_UP:
		y--
		if y < 0 {
			y = height - 1
		}
	case DIR_DOWN:
		y++
		if y >= height {
			y = 0
		}
	case DIR_LEFT:
		x--
		if x < 0 {
			x = width - 1
		}
	case DIR_RIGHT:
		x++
		if x >= width {
			x = 0
		}
	}

	return &Cell{
		X:    x,
		Y:    y,
		Char: cell.Char,
	}
}

func (c *Cell) Hit(other *Cell) bool {
	return c.X == other.X && c.Y == other.Y
}

func (e *Engine) slither() {
	grow := false
	if len(e.Snake.Body) < 8 {
		grow = true
	} else {
		e.NextGrowth--
		if e.NextGrowth == 0 {
			grow = true
			e.NextGrowth = e.Growth
		}
		e.NextSpeed--
		if e.NextSpeed == 0 {
			e.NextSpeed = e.Speed
			e.FPS++
			e.Refresh = time.NewTicker(time.Second / time.Duration(e.FPS))
		}
	}
	if !grow {
		e.Snake.Body = e.Snake.Body[1:]
	}
	head := e.Snake.Body[len(e.Snake.Body)-1]
	newhead := NewCellJoined(e.Width, e.Height, head, e.Direction)

	for _, cell := range e.Snake.Body {
		if newhead.Hit(cell) {
			for idx, _ := range e.Snake.Body {
				e.Snake.Body[idx].Char = RUNE_GAMEOVER
			}
			e.Refresh.Stop()
			return
		}
	}

	e.Snake.Body = append(e.Snake.Body, newhead)

	if newhead.Hit(e.Fruit) {
		glog.Infof("Eat Fruit %d %d", e.Fruit.X, e.Fruit.Y)
		e.Score++
		e.Fruit = NewFruit(e.Width, e.Height, e.Snake)
	}
}

func (e *Engine) render() {
	board := &Board{
		Width:  e.Width,
		Height: e.Height,
		Data:   make([]rune, e.Width*e.Height),
	}

	board.Data[(e.Fruit.Y*e.Width)+e.Fruit.X] = e.Fruit.Char

	board.Data[0] = 'S'
	board.Data[1] = 'c'
	board.Data[2] = 'o'
	board.Data[3] = 'r'
	board.Data[4] = 'e'
	board.Data[5] = ':'
	board.Data[6] = ' '
	val := fmt.Sprintf("%d", e.Score)
	for idx, c := range val {
		board.Data[7+idx] = c
	}

	for _, cell := range e.Snake.Body {
		board.Data[(cell.Y*e.Width)+cell.X] = cell.Char
	}

	e.Display.UpdateBoard(board)
}

func (e *Engine) run() {
	e.Refresh = time.NewTicker(time.Second / time.Duration(e.FPS))
	for {
		select {
		case <-e.Refresh.C:
			if e.Direction != DIR_NONE {
				e.slither()
			}
			e.render()
		case dir := <-e.Move:
			glog.Infof("Move %d", dir)
			ok := true
			if (dir == DIR_RIGHT && e.Direction == DIR_LEFT) ||
				(dir == DIR_LEFT && e.Direction == DIR_RIGHT) ||
				(dir == DIR_UP && e.Direction == DIR_DOWN) ||
				(dir == DIR_DOWN && e.Direction == DIR_UP) {
				ok = false
			}

			if ok {
				e.Direction = dir
			}
		case <-e.Quit:
			glog.Infof("Quit")
			return
		}
	}
}

func (e *Engine) Start() {
	go e.run()
}

func (e *Engine) Stop() {
	e.Quit <- true
}

func (g *Game) reset() {
	if g.Engine != nil {
		g.Quit()
		g.Play()
	}
}

func (g *Game) SetRect(x, y, width, height int) {
	g.Box.SetRect(x, y, width, height)

	_, _, width, height = g.GetInnerRect()
	width /= 2

	if g.Engine == nil || g.Engine.Width != width || g.Engine.Height != height {
		g.reset()
	}
}

func (g *Game) Play() {
	_, _, width, height := g.GetInnerRect()
	glog.Infof("play game %d %d", width, height)
	g.Engine = NewEngine(g, width/2, height, 8)
	g.Engine.Start()
}

func (g *Game) Quit() {
	g.Engine.Stop()
	g.Engine = nil
	g.Board = nil
}

func (g *Game) UpdateBoard(board *Board) {
	g.App.QueueUpdateDraw(func() {
		g.Board = board
	})
}

func (g *Game) InputHandler() func(event *tcell.EventKey, setFocus func(p tview.Primitive)) {
	return g.WrapInputHandler(func(event *tcell.EventKey, setFocus func(p tview.Primitive)) {
		// Process key event.
		switch event.Key() {
		case tcell.KeyUp:
			if g.Engine != nil {
				g.Engine.Move <- DIR_UP
			}
		case tcell.KeyDown:
			if g.Engine != nil {
				g.Engine.Move <- DIR_DOWN
			}
		case tcell.KeyLeft:
			if g.Engine != nil {
				g.Engine.Move <- DIR_LEFT
			}
		case tcell.KeyRight:
			if g.Engine != nil {
				g.Engine.Move <- DIR_RIGHT
			}
		case tcell.KeyEscape:
			g.reset()
		case tcell.KeyRune:
			if event.Rune() == 'q' {
				g.Quit()
				g.Listener.SnakeGameFinished()
			}
		}
	})
}

func (g *Game) Draw(screen tcell.Screen) {
	g.Box.Draw(screen)

	if g.Board == nil {
		return
	}
	// Prepare
	x, y, width, height := g.GetInnerRect()
	width /= 2

	if g.Board.Width != width || g.Board.Height != height {
		return
	}
	color := tcell.StyleDefault.Background(tview.Styles.PrimitiveBackgroundColor).Foreground(tview.Styles.PrimaryTextColor)
	for col := 0; col < width; col++ {
		for row := 0; row < height; row++ {
			val := g.Board.Data[(row*g.Board.Width)+col]
			if val != 0 {
				screen.SetContent(x+col*2, y+row, val, nil, color)
			}
		}
	}
}
