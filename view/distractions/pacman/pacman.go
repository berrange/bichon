// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package pacman

import (
	"fmt"
	"math/rand"
	"time"

	"github.com/gdamore/tcell"
	"github.com/golang/glog"
	"github.com/rivo/tview"
)

const maze = "############################" +
	"#            ##            #" +
	"# #### ##### ## ##### #### #" +
	"#X#### ##### ## ##### ####X#" +
	"#                          #" +
	"# #### ## ######## ## #### #" +
	"#      ##    ##    ##      #" +
	"###### ##### ## ##### ######" +
	".....# ##          ## #....." +
	".....# ## ###--### ## #....." +
	"###### ## # GGGG # ## ######" +
	"          # GGGG #          " +
	"###### ## # GGGG # ## ######" +
	".....# ## ######## ## #....." +
	".....# ##    P   . ## #....." +
	"###### ## ######## ## ######" +
	"#            ##   .  .     #" +
	"# #### ##### ## ##### #### #" +
	"#X  ##                ##  X#" +
	"### ## ## ######## ## ## ###" +
	"#      ##    ##    ##      #" +
	"# ########## ## ########## #" +
	"#                          #" +
	"############################"

const (
	RUNE_INVINCIBLE = '🐶'
	RUNE_WEAKENING  = '🐕'
	RUNE_PLAYER     = '🐩'
	RUNE_WALL       = '⬛'
	RUNE_GHOST      = '🐈'
	RUNE_DEAD       = '👹'
	RUNE_FRUIT      = '●'
	RUNE_MEAT       = '🍖'
)

type Direction int

const (
	DIR_NONE Direction = iota
	DIR_UP
	DIR_DOWN
	DIR_LEFT
	DIR_RIGHT
)

type Sprite struct {
	X         int
	Y         int
	Char      rune
	Direction Direction
}

type Board struct {
	Width  int
	Height int
	Data   []rune
}

type Display interface {
	UpdateBoard(board *Board, score int)
}

type Engine struct {
	Display    Display
	Board      Board
	FPS        int
	Score      int
	Invincible int
	Player     Sprite
	Ghosts     []Sprite
	Refresh    *time.Ticker
	Quit       chan bool
	Move       chan Direction
}

type Listener interface {
	SnakeGameFinished()
}

type Game struct {
	*tview.Box

	Listener Listener
	App      *tview.Application
	Board    *Board
	Score    int
	Engine   *Engine
}

func NewGame(app *tview.Application, listener Listener) *Game {
	return &Game{
		Box:      tview.NewBox(),
		Listener: listener,
		App:      app,
	}
}

func NewEngine(display Display, fps int) *Engine {
	engine := &Engine{
		Display: display,
		Board: Board{
			Width:  28,
			Height: 24,
			Data:   make([]rune, 28*24),
		},
		FPS:  fps,
		Quit: make(chan bool),
		Move: make(chan Direction),
	}

	cell := 0
	for y := 0; y < engine.Board.Height; y++ {
		for x := 0; x < engine.Board.Width; x++ {
			switch maze[cell] {
			case '#':
				engine.Board.Data[cell] = RUNE_WALL
			case 'G':
				engine.Ghosts = append(engine.Ghosts,
					Sprite{
						X:    x,
						Y:    y,
						Char: RUNE_GHOST,
					},
				)
			case 'P':
				engine.Player.X = x
				engine.Player.Y = y
				engine.Player.Char = RUNE_PLAYER
			case ' ':
				engine.Board.Data[cell] = RUNE_FRUIT
			case 'X':
				engine.Board.Data[cell] = RUNE_MEAT
			}
			cell++
		}
	}

	return engine
}

func (b *Board) translate(s *Sprite, dir Direction) (int, int) {
	newx := s.X
	newy := s.Y
	switch dir {
	case DIR_UP:
		newy--
	case DIR_DOWN:
		newy++
	case DIR_LEFT:
		newx--
	case DIR_RIGHT:
		newx++
	}

	if newx < 0 {
		newx = b.Width - 1
	}
	if newx == b.Width {
		newx = 0
	}
	if newy < 0 {
		newy = b.Height - 1
	}
	if newy == b.Height {
		newy = 0
	}

	return newx, newy
}

func (s *Sprite) Hit(other *Sprite) bool {
	return s.X == other.X && s.Y == other.Y
}

func (b *Board) isWall(x, y int) bool {
	cell := (y * b.Width) + x

	return b.Data[cell] == RUNE_WALL
}

func (b *Board) moveSprite(s *Sprite) bool {
	newx, newy := b.translate(s, s.Direction)

	if b.isWall(newx, newy) {
		return false
	}

	s.X = newx
	s.Y = newy

	return true
}

func (b *Board) renderSprite(sprite Sprite) {
	cell := (sprite.Y * b.Width) + sprite.X
	b.Data[cell] = sprite.Char
}

func (b *Board) consume(sprite Sprite) int {
	cell := (sprite.Y * b.Width) + sprite.X
	if b.Data[cell] == RUNE_FRUIT {
		b.Data[cell] = 0
		return 1
	}
	if b.Data[cell] == RUNE_MEAT {
		b.Data[cell] = 0
		return 50
	}
	return 0
}

func (b *Board) nextDir(s *Sprite, random, reversable bool) {
	if random && rand.Intn(10) < 6 {
		return
	}

	dirs := map[Direction]bool{
		DIR_UP:    true,
		DIR_DOWN:  true,
		DIR_LEFT:  true,
		DIR_RIGHT: true,
	}
	pairs := map[Direction]Direction{
		DIR_UP:    DIR_DOWN,
		DIR_DOWN:  DIR_UP,
		DIR_LEFT:  DIR_RIGHT,
		DIR_RIGHT: DIR_LEFT,
	}

	if !reversable {
		dirs[pairs[s.Direction]] = false
	}

	var options []Direction
	for dir, allow := range dirs {
		newx, newy := b.translate(s, dir)
		if allow && !b.isWall(newx, newy) {
			options = append(options, dir)
		}
	}

	s.Direction = options[rand.Intn(len(options))]
}

func (e *Engine) caught() bool {
	for idx, _ := range e.Ghosts {
		ghost := &e.Ghosts[idx]
		if e.Player.Hit(ghost) {
			e.Player.Char = RUNE_DEAD
			e.Refresh.Stop()
			return true
		}
	}
	return false
}

func (e *Engine) chase() {
	e.Board.moveSprite(&e.Player)
	if e.Invincible == 0 && e.caught() {
		return
	}
	health := e.Board.consume(e.Player)
	if health > 1 {
		e.Invincible = health
		e.Player.Char = RUNE_INVINCIBLE
	}
	e.Score += health

	for idx, _ := range e.Ghosts {
		ghost := &e.Ghosts[idx]
		if !e.Board.moveSprite(ghost) {
			e.Board.nextDir(ghost, false, true)
		} else {
			e.Board.nextDir(ghost, true, false)
		}
	}
	if e.Invincible == 0 && e.caught() {
		return
	}

	if e.Invincible > 0 {
		e.Invincible--
	}
	if e.Invincible == 0 {
		e.Player.Char = RUNE_PLAYER
	} else if e.Invincible == 10 {
		e.Player.Char = RUNE_WEAKENING
	}
}

func (e *Engine) render() {
	board := Board{
		Width:  e.Board.Width,
		Height: e.Board.Height,
		Data:   make([]rune, len(e.Board.Data)),
	}
	copy(board.Data, e.Board.Data)

	for _, ghost := range e.Ghosts {
		board.renderSprite(ghost)
	}
	board.renderSprite(e.Player)
	e.Display.UpdateBoard(&board, e.Score)
}

func (e *Engine) run() {
	e.Refresh = time.NewTicker(time.Second / time.Duration(e.FPS))
	for {
		select {
		case <-e.Refresh.C:
			e.chase()
			e.render()
		case dir := <-e.Move:
			glog.Infof("Move %d", dir)
			newx, newy := e.Board.translate(&e.Player, dir)
			if !e.Board.isWall(newx, newy) {
				e.Player.Direction = dir
			}
		case <-e.Quit:
			glog.Infof("Quit")
			return
		}
	}
}

func (e *Engine) Start() {
	go e.run()
}

func (e *Engine) Stop() {
	e.Quit <- true
}

func (g *Game) reset() {
	if g.Engine != nil {
		g.Quit()
		g.Play()
	}
}

func (g *Game) Play() {
	_, _, width, height := g.GetInnerRect()
	glog.Infof("play game %d %d", width, height)
	g.Engine = NewEngine(g, 6)
	g.Engine.Start()
}

func (g *Game) Quit() {
	g.Engine.Stop()
	g.Engine = nil
	g.Board = nil
}

func (g *Game) UpdateBoard(board *Board, score int) {
	g.App.QueueUpdateDraw(func() {
		g.Board = board
		g.Score = score
	})
}

func (g *Game) InputHandler() func(event *tcell.EventKey, setFocus func(p tview.Primitive)) {
	return g.WrapInputHandler(func(event *tcell.EventKey, setFocus func(p tview.Primitive)) {
		// Process key event.
		switch event.Key() {
		case tcell.KeyUp:
			if g.Engine != nil {
				g.Engine.Move <- DIR_UP
			}
		case tcell.KeyDown:
			if g.Engine != nil {
				g.Engine.Move <- DIR_DOWN
			}
		case tcell.KeyLeft:
			if g.Engine != nil {
				g.Engine.Move <- DIR_LEFT
			}
		case tcell.KeyRight:
			if g.Engine != nil {
				g.Engine.Move <- DIR_RIGHT
			}
		case tcell.KeyEscape:
			g.reset()
		case tcell.KeyRune:
			if event.Rune() == 'q' {
				g.Quit()
				g.Listener.SnakeGameFinished()
			}
		}
	})
}

func (g *Game) Draw(screen tcell.Screen) {
	g.Box.Draw(screen)

	if g.Board == nil {
		return
	}

	x, y, width, height := g.GetInnerRect()
	width /= 2

	if width < g.Board.Width || height < g.Board.Height {
		return
	}

	score := fmt.Sprintf("Score: %d", g.Score)
	tview.Print(screen, score, x, y, len(score), tview.AlignLeft, tview.Styles.PrimaryTextColor)
	x += (width - g.Board.Width)
	y += (height - g.Board.Height) / 2
	color := tcell.StyleDefault.Background(tview.Styles.PrimitiveBackgroundColor).Foreground(tview.Styles.PrimaryTextColor)
	for col := 0; col < g.Board.Width; col++ {
		for row := 0; row < g.Board.Height; row++ {
			val := g.Board.Data[(row*g.Board.Width)+col]
			if val != 0 {
				screen.SetContent(x+col*2, y+row, val, nil, color)
			}
		}
	}
}
