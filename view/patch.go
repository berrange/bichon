// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package view

import (
	"fmt"
	"strings"
	"time"

	"github.com/rivo/tview"

	"gitlab.com/bichon-project/bichon/diff"
	"gitlab.com/bichon-project/bichon/model"
)

type PatchRegion struct {
	IDStart int
	IDEnd   int
	Type    diff.DiffLineType
	OldFile string
	OldLine uint
	NewFile string
	NewLine uint
	Thread  string
}

func formatLine(text, prefix, color string, region *int) string {
	*region++

	return fmt.Sprintf("[\"l%d\"]%s[\"\"]%s%s[-:-:-]", *region, prefix, color, text)
}

func formatHunks(oldFile, newFile, commit string, region *int, threadsmap map[string][]model.CommentThread) ([]string, []PatchRegion, error) {
	colorPreamble :=
		GetStyleMarker(
			ELEMENT_DIFF_PREAMBLE_TEXT,
			ELEMENT_DIFF_PREAMBLE_FILL,
			ELEMENT_DIFF_PREAMBLE_ATTR)
	colorAdded :=
		GetStyleMarker(
			ELEMENT_DIFF_ADDED_TEXT,
			ELEMENT_DIFF_ADDED_FILL,
			ELEMENT_DIFF_ADDED_ATTR)
	colorRemoved :=
		GetStyleMarker(
			ELEMENT_DIFF_REMOVED_TEXT,
			ELEMENT_DIFF_REMOVED_FILL,
			ELEMENT_DIFF_REMOVED_ATTR)
	colorContext :=
		GetStyleMarker(
			ELEMENT_DIFF_CONTEXT_TEXT,
			ELEMENT_DIFF_CONTEXT_FILL,
			ELEMENT_DIFF_CONTEXT_ATTR)

	hunks, err := diff.ParseUnifiedDiffHunks(commit)
	if err != nil {
		return []string{}, []PatchRegion{}, err
	}

	lines := make([]string, 0)
	oldMax := 0
	newMax := 0
	for _, hunk := range hunks {
		for _, line := range hunk.Lines {
			if line.Type == diff.DIFF_LINE_ADDED ||
				line.Type == diff.DIFF_LINE_CONTEXT {
				newMax = line.NewLine
			}
			if line.Type == diff.DIFF_LINE_REMOVED ||
				line.Type == diff.DIFF_LINE_CONTEXT {
				oldMax = line.OldLine
			}
		}
	}
	oldDigits := len(fmt.Sprintf("%d", oldMax))
	newDigits := len(fmt.Sprintf("%d", newMax))

	fmtOldLine := fmt.Sprintf("%%%dd", oldDigits)
	fmtNewLine := fmt.Sprintf("%%%dd", newDigits)
	fmtOldPad := fmt.Sprintf("%%%ds", oldDigits)
	fmtNewPad := fmt.Sprintf("%%%ds", newDigits)

	regions := make([]PatchRegion, 0)
	prevType := diff.DiffLineType(-1)
	for _, hunk := range hunks {
		lines = append(lines, formatLine(hunk.FormatContext(), " ", colorPreamble, region))

		for _, line := range hunk.Lines {
			var prefix string
			var color string

			if line.Type == diff.DIFF_LINE_CONTEXT {
				prefix = fmt.Sprintf(" "+fmtOldLine+" "+fmtNewLine, line.OldLine, line.NewLine)
				color = colorContext
			} else if line.Type == diff.DIFF_LINE_REMOVED {
				prefix = fmt.Sprintf(" "+fmtOldLine+" "+fmtNewPad, line.OldLine, "")
				color = colorRemoved
			} else if line.Type == diff.DIFF_LINE_ADDED {
				prefix = fmt.Sprintf(" "+fmtOldPad+" "+fmtNewLine, "", line.NewLine)
				color = colorAdded
			}

			lines = append(lines, formatLine(" "+tview.Escape(line.Text), prefix, color, region))

			if line.Type != prevType {
				info := PatchRegion{
					IDStart: *region,
					IDEnd:   *region,
					Type:    line.Type,
					OldFile: oldFile,
					NewFile: newFile,
					OldLine: uint(line.OldLine),
					NewLine: uint(line.NewLine),
				}
				prevType = line.Type
				regions = append(regions, info)
			} else {
				info := &regions[len(regions)-1]
				info.IDEnd = *region
			}

			key := fmt.Sprintf("%s:%d,%s:%d", oldFile, line.OldLine, newFile, line.NewLine)
			threads, ok := threadsmap[key]
			if ok {
				indent := strings.Repeat(" ", newDigits+oldDigits+3) + "|"
				clines, cregions := FormatThreads(threads, false, region, indent)
				lines = append(lines, clines...)
				regions = append(regions, cregions...)
			}
		}
	}

	return lines, regions, nil
}

func formatDiff(diff *model.Diff, region *int, threadsmap map[string][]model.CommentThread) ([]string, []PatchRegion, error) {
	colorPreamble :=
		GetStyleMarker(
			ELEMENT_DIFF_PREAMBLE_TEXT,
			ELEMENT_DIFF_PREAMBLE_FILL,
			ELEMENT_DIFF_PREAMBLE_ATTR)
	oldFile := diff.OldFile
	newFile := diff.NewFile
	if oldFile == "" {
		oldFile = "/dev/null"
	}
	if newFile == "" {
		newFile = "/dev/null"
	}

	lines := []string{
		formatLine(fmt.Sprintf("diff a/%s b/%s", tview.Escape(oldFile), tview.Escape(newFile)),
			" ", colorPreamble, region),
	}

	var action string
	if diff.CreatedFile {
		action = fmt.Sprintf("new file mode %s", diff.NewMode)
	} else if diff.RenamedFile {
		action = fmt.Sprintf("renamed file mode %s - %s", diff.OldMode, diff.NewMode)
	} else if diff.DeletedFile {
		action = fmt.Sprintf("delete file mode %s", diff.OldMode)
	} else {
		action = fmt.Sprintf("file mode %s - %s", diff.OldMode, diff.NewMode)
	}
	lines = append(lines,
		formatLine(action, " ", colorPreamble, region),
		formatLine(fmt.Sprintf("--- a/%s", tview.Escape(oldFile)), " ", colorPreamble, region),
		formatLine(fmt.Sprintf("+++ b/%s", tview.Escape(newFile)), " ", colorPreamble, region))

	hunks, regions, err := formatHunks(oldFile, newFile, diff.Content, region, threadsmap)
	if err != nil {
		return []string{}, []PatchRegion{}, err
	}
	lines = append(lines, hunks...)

	return lines, regions, nil
}

func buildCommentMap(commit *model.Commit, threads []model.CommentThread) (filethreads map[string][]model.CommentThread) {
	filethreads = make(map[string][]model.CommentThread)
	for _, thread := range threads {
		if len(thread.Comments) == 0 {
			continue
		}
		comment := thread.Comments[0]
		if comment.Context == nil {
			continue
		}
		ctx := comment.Context

		if ctx.HeadHash != commit.Hash {
			continue
		}

		key := fmt.Sprintf("%s:%d,%s:%d", ctx.OldFile, ctx.OldLine, ctx.NewFile, ctx.NewLine)
		_, ok := filethreads[key]
		if !ok {
			filethreads[key] = []model.CommentThread{}
		}
		filethreads[key] = append(filethreads[key], thread)
	}
	return
}

func FormatCommit(commit *model.Commit, threads []model.CommentThread) ([]string, []PatchRegion, error) {
	colorHeader :=
		GetStyleMarker(
			ELEMENT_SUMMARY_HEADER_TEXT,
			ELEMENT_SUMMARY_HEADER_FILL,
			ELEMENT_SUMMARY_HEADER_ATTR)
	colorDate :=
		GetStyleMarker(
			ELEMENT_SUMMARY_DATE_TEXT,
			ELEMENT_SUMMARY_DATE_FILL,
			ELEMENT_SUMMARY_DATE_ATTR)
	colorAuthor :=
		GetStyleMarker(
			ELEMENT_SUMMARY_AUTHOR_TEXT,
			ELEMENT_SUMMARY_AUTHOR_FILL,
			ELEMENT_SUMMARY_AUTHOR_ATTR)

	region := 0

	threadsmap := buildCommentMap(commit, threads)

	lines := []string{
		formatLine(fmt.Sprintf("   Commit: %s", commit.Hash), " ", colorHeader, &region),
		formatLine(fmt.Sprintf("   Author: %s%s <%s>",
			colorAuthor,
			tview.Escape(commit.Author.Name),
			tview.Escape(commit.Author.Email)), " ", colorHeader, &region),
		formatLine(fmt.Sprintf("  Created: %s%s",
			colorDate,
			commit.CreatedAt.Format(time.RFC1123)), " ", colorHeader, &region),
		formatLine(fmt.Sprintf("Committer: %s%s <%s>",
			colorAuthor,
			tview.Escape(commit.Committer.Name),
			tview.Escape(commit.Committer.Email)), " ", colorHeader, &region),
		formatLine(fmt.Sprintf("  Updated: %s%s",
			colorDate,
			commit.UpdatedAt.Format(time.RFC1123)), " ", colorHeader, &region),
		formatLine("", " ", colorHeader, &region),
	}

	for _, line := range strings.Split(tview.Escape(commit.Message), "\n") {
		lines = append(lines, formatLine(tview.Escape(line), " ", "", &region))
	}

	var allregions []PatchRegion
	if commit.Metadata.Partial {
		lines = append(lines, "Please wait, patch diff is being loaded...")
	} else {
		for _, diff := range commit.Diffs {
			difflines, regions, err := formatDiff(&diff, &region, threadsmap)
			if err != nil {
				return []string{}, []PatchRegion{}, err
			}
			lines = append(lines, difflines...)
			allregions = append(allregions, regions...)
		}
	}

	return lines, allregions, nil
}
