// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package view

import (
	"github.com/rivo/tview"
)

func TrimEllipsisFront(data string, maxlen int) string {
	datalen := len(data)
	if datalen <= maxlen {
		return data
	}

	offset := (datalen - maxlen) + 1

	data = data[offset:]
	for {
		if data[0] == ' ' {
			data = data[1:]
		} else {
			break
		}
	}
	return "…" + data
}

func Modal(p tview.Primitive, width, height int) tview.Primitive {
	return tview.NewFlex().
		AddItem(nil, 0, 1, false).
		AddItem(tview.NewFlex().SetDirection(tview.FlexRow).
			AddItem(nil, 0, 1, false).
			AddItem(p, height, 1, true).
			AddItem(nil, 0, 1, false), width, 1, true).
		AddItem(nil, 0, 1, false)
}
