// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package view

import (
	"fmt"
	"strings"

	"github.com/gdamore/tcell"
	"github.com/golang/glog"
	"github.com/rivo/tview"

	"gitlab.com/bichon-project/bichon/controller"
	"gitlab.com/bichon-project/bichon/model"
	"gitlab.com/bichon-project/bichon/view/distractions/pacman"
	"gitlab.com/bichon-project/bichon/view/distractions/snake"
)

type Display struct {
	*tview.Application

	Engine    controller.Engine
	MergeReqs model.MergeReqList
	Repos     []model.Repo

	Layout       *tview.Flex
	KeyShortcuts *tview.TextView
	StatusBar    *tview.TextView
	Messages     *MessageBar
	Pages        *tview.Pages
	Index        *IndexPage
	Detail       *DetailPage
	Projects     *ProjectsPage
	AddProject   *AddProjectPage
	ThisPage     Page

	Snake  *snake.Game
	Pacman *pacman.Game

	FormActive bool
	SortForm   *SortForm
	FilterForm *FilterForm
}

func NewDisplay(engine controller.Engine) (*Display, error) {
	err := LoadStyleConfig()
	if err != nil {
		return nil, err
	}

	tview.TabSize = 8
	tview.Styles.PrimitiveBackgroundColor = GetStyleColor(ELEMENT_PRIMITIVE_FILL)
	tview.Styles.ContrastBackgroundColor = GetStyleColor(ELEMENT_CONTRAST_FILL)
	tview.Styles.MoreContrastBackgroundColor = GetStyleColor(ELEMENT_MORE_CONTRAST_FILL)
	tview.Styles.PrimaryTextColor = GetStyleColor(ELEMENT_PRIMARY_TEXT)
	tview.Styles.SecondaryTextColor = GetStyleColor(ELEMENT_SECONDARY_TEXT)
	tview.Styles.TertiaryTextColor = GetStyleColor(ELEMENT_TERTIARY_TEXT)
	tview.Styles.InverseTextColor = GetStyleColor(ELEMENT_INVERSE_TEXT)
	tview.Styles.ContrastSecondaryTextColor = GetStyleColor(ELEMENT_CONTRAST_SECONDARY_TEXT)
	tview.Styles.BorderColor = GetStyleColor(ELEMENT_BORDER)
	tview.Styles.TitleColor = GetStyleColor(ELEMENT_TITLE)
	tview.Styles.GraphicsColor = GetStyleColor(ELEMENT_GRAPHICS)

	msgs := NewMessageBar()
	display := &Display{
		Application:  tview.NewApplication(),
		Engine:       engine,
		Layout:       tview.NewFlex(),
		KeyShortcuts: tview.NewTextView().SetDynamicColors(true),
		StatusBar:    tview.NewTextView().SetDynamicColors(true),
		Messages:     msgs,
		Pages:        tview.NewPages(),
	}

	display.Index = NewIndexPage(display.Application, display)
	display.Detail = NewDetailPage(display.Application, display)
	display.Projects = NewProjectsPage(display.Application, display)
	display.AddProject = NewAddProjectPage(display.Application, display)
	display.SortForm = NewSortForm(display, SORT_FORM_ORDER_AGE, false)
	display.FilterForm = NewFilterForm(display)
	display.Snake = snake.NewGame(display.Application, display)
	display.Pacman = pacman.NewGame(display.Application, display)

	display.SetRoot(display.Layout, true)

	display.Layout.SetDirection(tview.FlexRow).
		AddItem(display.KeyShortcuts, 1, 1, false).
		AddItem(display.Pages, 0, 1, false).
		AddItem(display.StatusBar, 1, 1, false).
		AddItem(display.Messages.Text, 1, 1, false)

	display.Pages.AddPage(display.Index.GetName(), display.Index, true, true)
	display.Pages.AddPage(display.Detail.GetName(), display.Detail, true, true)
	display.Pages.AddPage(display.Projects.GetName(), display.Projects, true, true)
	display.Pages.AddPage(display.AddProject.GetName(), display.AddProject, true, true)
	display.Pages.AddPage("sort-form", display.SortForm, true, true)
	display.Pages.AddPage("filter-form", display.FilterForm, true, true)
	display.Pages.AddPage("snake", display.Snake, true, true)
	display.Pages.AddPage("pacman", display.Pacman, true, true)

	display.MergeReqs.Filter = display.FilterForm.GetMergeReqFilter()
	display.MergeReqs.Sorter = display.SortForm.GetMergeReqSorter()

	display.StatusBar.SetText(fmt.Sprintf("[%s:%s]--- Bichon",
		GetStyleColorName(ELEMENT_STATUS_TEXT),
		GetStyleColorName(ELEMENT_STATUS_FILL)) +
		strings.Repeat(" ", 500))

	display.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		if display.FormActive {
			return event
		}
		switch event.Key() {
		case tcell.KeyF1:
			display.Pages.SendToFront("snake")
			display.Snake.Play()
			display.Application.SetFocus(display.Snake)
			display.FormActive = true
			return nil

		case tcell.KeyF2:
			display.Pages.SendToFront("pacman")
			display.Pacman.Play()
			display.Application.SetFocus(display.Pacman)
			display.FormActive = true
			return nil
		}
		return display.ThisPage.HandleInput(event)
	})

	return display, nil
}

func (display *Display) SortFormConfirm(sorter model.MergeReqSorter) {
	display.MergeReqs.Sorter = sorter
	display.MergeReqs.ReSort()
	display.Index.Refresh(display.Application, display.MergeReqs.Active)

	display.SortFormCancel()
}

func (display *Display) SortFormCancel() {
	display.Pages.SendToBack("sort-form")
	display.Application.SetFocus(display.Index)
	display.FormActive = false
}

func (display *Display) IndexPagePickSort() {
	glog.Info("Show sort")
	display.FormActive = true
	display.Pages.SendToFront("sort-form")
	display.Pages.ShowPage("sort-form")
	display.Application.SetFocus(display.SortForm)
}

func (display *Display) FilterFormConfirm(filter model.MergeReqFilter) {
	display.MergeReqs.Filter = filter
	display.MergeReqs.ReFilter()
	display.Index.Refresh(display.Application, display.MergeReqs.Active)

	display.FilterFormCancel()
}

func (display *Display) FilterFormCancel() {
	display.Pages.SendToBack("filter-form")
	display.Application.SetFocus(display.Index)
	display.FormActive = false
}

func (display *Display) IndexPagePickFilter() {
	glog.Info("Show filter")
	display.FormActive = true
	display.Pages.SendToFront("filter-form")
	display.Pages.ShowPage("filter-form")
	display.Application.SetFocus(display.FilterForm)
}

func (display *Display) IndexPageQuit() {
	display.Stop()
}

func (display *Display) IndexPageViewMergeRequest(mreq model.MergeReq) {
	display.switchToPage(display.Detail)
	display.Detail.Refresh(display.Application, &mreq)
	display.Engine.MarkRead(mreq)
}

func (display *Display) IndexPageViewProjects() {
	display.switchToPage(display.Projects)
}

func (display *Display) IndexPageRefreshMergeRequests() {
	display.Engine.RefreshRepos()
}

func (display *Display) DetailPageQuit() {
	display.switchToPage(display.Index)
}

func (display *Display) DetailPageRefreshMergeRequest(mreq *model.MergeReq) {
	display.Engine.RefreshMergeRequest(*mreq)
}

func (display *Display) DetailPageAddMergeReqComment(mreq *model.MergeReq, text string, context *model.CommentContext) {
	if context == nil {
		display.Engine.AddMergeRequestComment(*mreq, text)
	} else {
		display.Engine.AddMergeRequestThread(*mreq, text, context)
	}
}

func (display *Display) DetailPageAddMergeReqReply(mreq *model.MergeReq, thread, text string) {
	display.Engine.AddMergeRequestReply(*mreq, thread, text)
}

func (display *Display) DetailPageLoadMergeReqCommitDiffs(mreq *model.MergeReq, commit *model.Commit) {
	display.Engine.LoadMergeRequestCommitDiffs(*mreq, *commit)
}

func (display *Display) DetailPageAcceptMergeReq(mreq *model.MergeReq) {
	display.Engine.AcceptMergeRequest(*mreq)
}

func (display *Display) DetailPageApproveMergeReq(mreq *model.MergeReq) {
	display.Engine.ApproveMergeRequest(*mreq)
}

func (display *Display) DetailPageUnapproveMergeReq(mreq *model.MergeReq) {
	display.Engine.UnapproveMergeRequest(*mreq)
}

func (display *Display) ProjectsPageQuit() {
	display.switchToPage(display.Index)
}

func (display *Display) ProjectsPageAddRepo() {
	glog.Info("Showing add projects page")
	display.switchToPage(display.AddProject)
}

func (display *Display) AddProjectPageCancel() {
	display.switchToPage(display.Projects)
}

func (display *Display) AddProjectPageConfirm(repo model.Repo) {
	display.Engine.AddRepository(repo)
	display.switchToPage(display.Index)
}

func (display *Display) SnakeGameFinished() {
	display.switchToPage(display.Index)
}

func (display *Display) PacmanGameFinished() {
	display.switchToPage(display.Index)
}

func (display *Display) Status(msg string) {
	display.Application.QueueUpdateDraw(func() {
		display.Messages.Info(msg)
	})
}

func (display *Display) MergeRequestNotify(mreq *model.MergeReq) {
	glog.Infof("Received merge request %s", mreq.String())
	display.MergeReqs.Insert(mreq)

	glog.Info("Updating index page")
	display.Index.Refresh(display.Application, display.MergeReqs.Active)

	glog.Info("Updating detail page")
	thatmreq := display.Index.GetSelectedMergeRequest()
	if thatmreq != nil && thatmreq.Equal(mreq) {
		display.Detail.Refresh(display.Application, mreq)
	}
}

func (display *Display) RepoAdded(repo model.Repo) {
	display.Repos = append(display.Repos, repo)
	display.Projects.Refresh(display.Application, display.Repos)
	display.FilterForm.Refresh(display.Application, display.Repos)
}

func (display *Display) RepoRemoved(repo model.Repo) {
	for idx, thisrepo := range display.Repos {
		if thisrepo.Equal(&repo) {
			display.Repos = append(display.Repos[0:idx],
				display.Repos[idx+1:]...)
		}
	}
	display.Projects.Refresh(display.Application, display.Repos)
	display.FilterForm.Refresh(display.Application, display.Repos)
}

func (display *Display) switchToPage(page Page) {
	display.Pages.SendToFront(page.GetName())
	display.FormActive = false
	display.ThisPage = page
	display.ThisPage.Activate()
	shortcuts := fmt.Sprintf("[%s:%s]",
		GetStyleColorName(ELEMENT_SHORTCUTS_TEXT),
		GetStyleColorName(ELEMENT_SHORTCUTS_FILL)) +
		display.ThisPage.GetKeyShortcuts() +
		strings.Repeat(" ", 500)
	display.KeyShortcuts.SetText(shortcuts)
}

func (display *Display) Run() {
	display.Engine.Run(display)
	if len(display.Repos) == 0 {
		display.switchToPage(display.AddProject)
		display.AddProject.LoadLocalProject()
	} else {
		display.switchToPage(display.Index)
	}
	display.Application.Run()
}
