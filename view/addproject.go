// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package view

import (
	"github.com/gdamore/tcell"
	"github.com/golang/glog"
	"github.com/rivo/tview"

	"gitlab.com/bichon-project/bichon/model"
	"gitlab.com/bichon-project/bichon/security"
)

type AddProjectPageListener interface {
	AddProjectPageCancel()
	AddProjectPageConfirm(repo model.Repo)
}

type AddProjectPage struct {
	*tview.Frame

	Application *tview.Application
	Listener    AddProjectPageListener
	Details     *tview.Form
	Directory   *tview.InputField
	Remote      *tview.InputField
	Server      *tview.InputField
	Project     *tview.InputField
	Token       *tview.InputField
	Global      *tview.Checkbox
}

func addInputField(form *tview.Form, label, value string, width int) *tview.InputField {
	form.AddInputField(label, value, width, nil, nil)

	item := form.GetFormItemByLabel(label)

	input, _ := item.(*tview.InputField)

	return input
}

func addCheckbox(form *tview.Form, label string, value bool) *tview.Checkbox {
	form.AddCheckbox(label, value, nil)

	item := form.GetFormItemByLabel(label)

	input, _ := item.(*tview.Checkbox)

	return input
}

func NewAddProjectPage(app *tview.Application, listener AddProjectPageListener) *AddProjectPage {
	details := tview.NewForm()

	directory := addInputField(details, "Directory", "", 40)
	remote := addInputField(details, "Remote", "origin", 20)
	server := addInputField(details, "Server", "", 20)
	project := addInputField(details, "Project", "", 20)
	token := addInputField(details, "API Token", "", 40)
	global := addCheckbox(details, "Use token for all repos on this server", true)

	details.SetBorder(true).
		SetTitle("Add a new project")

	layout := tview.NewFrame(details).
		SetBorders(0, 0, 0, 0, 0, 0)

	page := &AddProjectPage{
		Frame: layout,

		Application: app,
		Listener:    listener,
		Details:     details,
		Directory:   directory,
		Remote:      remote,
		Server:      server,
		Project:     project,
		Token:       token,
		Global:      global,
	}

	details.AddButton("Save", page.confirmButton)
	details.AddButton("Cancel", page.cancelButton)
	page.Directory.SetChangedFunc(page.autoCompleteRepo)
	page.Remote.SetChangedFunc(page.autoCompleteRepo)

	project.SetChangedFunc(page.autoCompleteToken)

	return page
}

func (page *AddProjectPage) autoCompleteRepo(text string) {
	dir := page.Directory.GetText()
	remote := page.Remote.GetText()

	glog.Infof("Auto complete repo for '%s' & '%s'", dir, remote)

	if dir == "" || remote == "" {
		return
	}

	repo, err := model.NewRepoForDirectory(dir, remote)
	if err != nil {
		return
	}

	page.Server.SetText(repo.Server)
	page.Project.SetText(repo.Project)
}

func (page *AddProjectPage) autoCompleteToken(text string) {
	server := page.Server.GetText()
	project := page.Project.GetText()

	glog.Infof("Auto complete token for '%s' & '%s'", server, project)

	token, err := security.FetchToken(server, project)
	if err == nil {
		glog.Info("Setting token")
		page.Token.SetText(token)
	}
}

func (page *AddProjectPage) GetName() string {
	return "addproject"
}

func (page *AddProjectPage) GetKeyShortcuts() string {
	return "q:Cancel <enter>:Confirm ?:Help"
}

func (page *AddProjectPage) Activate() {
	page.Details.SetFocus(0)
	page.Application.SetFocus(page.Details)
}

func (page *AddProjectPage) HandleInput(event *tcell.EventKey) *tcell.EventKey {
	return event
}

func (page *AddProjectPage) clearText() {
	page.Directory.SetText("")
	page.Remote.SetText("origin")
	page.Server.SetText("")
	page.Project.SetText("")
	page.Token.SetText("")
	page.Global.SetChecked(true)
}

func (page *AddProjectPage) confirmButton() {
	dir := page.Directory.GetText()
	remote := page.Remote.GetText()
	server := page.Server.GetText()
	proj := page.Project.GetText()
	token := page.Token.GetText()
	global := page.Global.IsChecked()

	if proj[0] == '/' {
		proj = proj[1:]
	}

	if global {
		security.StoreToken(server, "", token)
	} else {
		security.StoreToken(server, proj, token)
	}

	repo := model.NewRepo(dir, remote, server, proj)

	page.clearText()
	page.Listener.AddProjectPageConfirm(*repo)
}

func (page *AddProjectPage) cancelButton() {
	page.clearText()
	page.Listener.AddProjectPageCancel()
}

func (page *AddProjectPage) LoadLocalProject() {
	gitdir, err := model.FindGitDir()

	if err == nil {
		page.Directory.SetText(gitdir)
	}
}
