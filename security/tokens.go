// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package security

import (
	"fmt"

	"github.com/golang/glog"
	"github.com/zalando/go-keyring"
)

func FetchToken(server, project string) (string, error) {
	keyname := fmt.Sprintf("%s/%s", server, project)

	glog.Infof("Getting token '%s'", keyname)
	data, err := keyring.Get("bichon", keyname)
	if err != nil {
		glog.Infof("Getting token '%s'", server)
		data, err = keyring.Get("bichon", server)
	}

	glog.Infof("Ok '%s'", data)
	return data, err
}

func StoreToken(server, project, data string) error {
	var keyname string
	if project == "" {
		keyname = server
	} else {
		keyname = fmt.Sprintf("%s/%s", server, project)
	}

	glog.Infof("Setting token '%s'", keyname)
	return keyring.Set("bichon", keyname, data)
}
