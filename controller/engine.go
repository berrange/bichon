// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package controller

import (
	"crypto/tls"
	"fmt"
	"os"
	"time"

	"github.com/casimir/xdg-go"
	"github.com/go-ini/ini"
	"github.com/golang/glog"

	"gitlab.com/bichon-project/bichon/cache"
	"gitlab.com/bichon-project/bichon/model"
	"gitlab.com/bichon-project/bichon/source"
)

type Engine interface {
	Run(listener Listener)

	AddRepository(repo model.Repo)
	RemoveRepository(repo model.Repo)

	RefreshRepos()
	RefreshMergeRequest(mreq model.MergeReq)

	LoadMergeRequestCommitDiffs(mreq model.MergeReq, commit model.Commit)

	AddMergeRequestComment(mreq model.MergeReq, text string)
	AddMergeRequestThread(mreq model.MergeReq, text string, context *model.CommentContext)
	AddMergeRequestReply(mreq model.MergeReq, thread, text string)

	MarkRead(mreq model.MergeReq)

	AcceptMergeRequest(mreq model.MergeReq)
	ApproveMergeRequest(mreq model.MergeReq)
	UnapproveMergeRequest(mreq model.MergeReq)
}

type engineJob interface {
	Run(engine *engineImpl)
}

type engineImplRepo struct {
	Repo model.Repo
	// Once added the MergeReq should be treated as immutable
	// To change it, create a copy, change the copy and assign
	// that copy back into this map
	MergeRequests map[uint]*model.MergeReq
	Removed       bool
	Source        source.Source
}

type engineAddRepoJob struct {
	Repo model.Repo
}

type engineRemoveRepoJob struct {
	Repo model.Repo
}

type engineRefreshReposJob struct {
}

type engineRefreshMergeRequestJob struct {
	MergeReq model.MergeReq
}

type engineLoadMergeRequestCommitDiffsJob struct {
	MergeReq model.MergeReq
	Commit   model.Commit
}

type engineAddMergeRequestCommentJob struct {
	MergeReq model.MergeReq
	Text     string
}

type engineAddMergeRequestThreadJob struct {
	MergeReq model.MergeReq
	Text     string
	Context  *model.CommentContext
}

type engineAddMergeRequestReplyJob struct {
	MergeReq model.MergeReq
	Thread   string
	Text     string
}

type engineMergeRequestMarkReadJob struct {
	MergeReq model.MergeReq
}

type engineMergeRequestAcceptJob struct {
	MergeReq model.MergeReq
}

type engineMergeRequestApproveJob struct {
	MergeReq model.MergeReq
}

type engineMergeRequestUnapproveJob struct {
	MergeReq model.MergeReq
}

type engineImpl struct {
	// Immutable
	RefreshInterval time.Duration
	XDG             xdg.App
	TLSConfig       *tls.Config

	// Immutable once updateLoop goroutine starts
	Listener Listener

	// Only access from updateLoop goroutine
	Cache cache.Cache
	Repos []engineImplRepo

	// For comms with updateLoop goroutine
	RefreshJob *time.Timer
	Jobs       chan engineJob
}

func NewEngine(tlscfg *tls.Config) (Engine, error) {
	xdgapp := xdg.App{
		Name: "bichon",
	}
	mreqcache := cache.NewFileCache(xdgapp.CachePath("merge-requests"))

	engine := &engineImpl{
		RefreshInterval: time.Minute * 15,
		XDG:             xdgapp,
		TLSConfig:       tlscfg,

		Cache: mreqcache,

		RefreshJob: time.NewTimer(0), // Immediate expire first time
		Jobs:       make(chan engineJob, 1000),
	}

	err := engine.loadRepos()
	if err != nil {
		return nil, err
	}

	return engine, nil
}

func (engine *engineImpl) loadRepos() error {
	projectspath := engine.XDG.ConfigPath("projects.ini")

	cfg, err := ini.Load(projectspath)
	if err != nil {
		if os.IsNotExist(err) {
			return nil
		}
		return err
	}

	glog.Infof("Loading repos from %s", projectspath)
	for _, sec := range cfg.Sections() {
		if sec.Name() == "DEFAULT" {
			continue
		}
		dir := sec.Name()

		remote := sec.Key("remote").String()
		server := sec.Key("server").String()
		project := sec.Key("project").String()

		repo := model.NewRepo(dir, remote, server, project)

		engine.addRepo(*repo)
	}

	return nil
}

func (engine *engineImpl) saveRepos() {
	projectspath := engine.XDG.ConfigPath("projects.ini")

	cfg := ini.Empty()

	for _, enginerepo := range engine.Repos {
		sec, _ := cfg.NewSection(enginerepo.Repo.Directory)

		sec.NewKey("remote", enginerepo.Repo.Remote)
		sec.NewKey("server", enginerepo.Repo.Server)
		sec.NewKey("project", enginerepo.Repo.Project)
	}

	err := cfg.SaveTo(projectspath + ".new")
	if err != nil {
		return
	}

	os.Rename(projectspath+".new", projectspath)
}

func (engine *engineImpl) loadMergeRequestCache(enginerepo *engineImplRepo) {
	engine.Listener.Status(fmt.Sprintf("Loading merge request cache"))
	mreqids, _ := engine.Cache.ListMergeRequests(&enginerepo.Repo)
	engine.Listener.Status(fmt.Sprintf("Loading %d merge requests from cache", len(mreqids)))

	for idx, id := range mreqids {
		engine.Listener.Status(fmt.Sprintf("Loading merge request %d from cache [%d/%d]",
			id, idx+1, len(mreqids)))
		mreq, err := engine.Cache.LoadMergeRequest(&enginerepo.Repo, id)
		if err != nil {
			continue
		}

		enginerepo.MergeRequests[mreq.ID] = mreq
		engine.Listener.MergeRequestNotify(mreq)
	}
	engine.Listener.Status("")
}

func (engine *engineImpl) refreshMergeRequest(enginerepo *engineImplRepo, mreq *model.MergeReq) {
	versions, err := enginerepo.Source.GetVersions(mreq)
	if err != nil {
		engine.Listener.Status(fmt.Sprintf("Unable to fetch merge request versions: %s", err))
		return
	}

	for idx, _ := range versions {
		ver := &versions[idx]
		for _, oldver := range mreq.Versions {
			if ver.Version == oldver.Version {
				ver.Patches = oldver.Patches
			}
		}

		// We are only loading the most recent version by default
		// We'll add support for lazy loading older versions
		// once we add UI for choosing to display other versions
		if idx == (len(versions) - 1) {
			patches, err := enginerepo.Source.GetPatches(mreq, ver)
			if err != nil {
				engine.Listener.Status(fmt.Sprintf("Unable to fetch merge request patches: %s", err))
				continue
			}

			ver.Patches = patches
			ver.Metadata.Partial = false
		}
	}

	mreq.Versions = versions

	threads, err := enginerepo.Source.GetMergeRequestThreads(mreq)
	if err != nil {
		engine.Listener.Status(fmt.Sprintf("Unable to fetch merge request threads: %s", err))
		return
	}

	mreq.Threads = threads

	mreq.Metadata.Partial = false

	err = engine.Cache.SaveMergeRequest(mreq)
	if err != nil {
		engine.Listener.Status(fmt.Sprintf("Cannot save merge request %d to cache: %s",
			mreq.ID, err))
	}
}

func (engine *engineImpl) refreshMergeRequests(enginerepo *engineImplRepo) {
	glog.Infof("Refresh merged requests %s", enginerepo.Repo.String())
	engine.Listener.Status(fmt.Sprintf("Querying current merge requests %s", enginerepo.Repo.String()))
	mreqs, err := enginerepo.Source.GetMergeRequests()
	if err != nil {
		engine.Listener.Status(fmt.Sprintf("Unable to query merge requests: %s", err))
		return
	}

	mreqsToUpdate := make([]model.MergeReq, 0)
	for _, mreq := range mreqs {
		cachedmreq, ok := enginerepo.MergeRequests[mreq.ID]

		if ok {
			// We never want to loose info, so we'll copy the cached
			// versions and refresh it later
			mreq.Versions = cachedmreq.Versions
			mreq.Metadata.Status = model.STATUS_UPDATED

			if cachedmreq.UpdatedAt.Before(mreq.UpdatedAt) || cachedmreq.Metadata.Partial {
				glog.Infof("Mreq %s is outdated", mreq.String())
				mreqsToUpdate = append(mreqsToUpdate, mreq)
			} else {
				glog.Infof("Mreq %s is up2date", mreq.String())
			}
		} else {
			glog.Infof("Mreq %s is not cached", mreq.String())
			mreqsToUpdate = append(mreqsToUpdate, mreq)
		}
	}
	mreqs = mreqsToUpdate

	if len(mreqs) > 0 {
		engine.Listener.Status(fmt.Sprintf("Fetching %d merge requests", len(mreqs)))
	}
	for idx, _ := range mreqs {
		mreq := &mreqs[idx]
		engine.Listener.Status(fmt.Sprintf("Fetching merge request %d from source [%d/%d]",
			mreq.ID, idx+1, len(mreqs)))
		engine.refreshMergeRequest(enginerepo, mreq)

		enginerepo.MergeRequests[mreq.ID] = mreq
		engine.Listener.MergeRequestNotify(mreq)
	}
	engine.Listener.Status("")
}

func (engine *engineImpl) refreshJob() {
	glog.Info("Periodic refresh job running")
	for idx, _ := range engine.Repos {
		repo := &engine.Repos[idx]
		glog.Infof("Refreshing repo %s", repo.Repo.String())
		engine.refreshMergeRequests(repo)
	}
}

func (engine *engineImpl) addRepo(repo model.Repo) (*engineImplRepo, error) {
	glog.Infof("Adding repo %s", repo.String())

	source, err := source.NewGitLabForRepo(engine.TLSConfig, &repo)
	if err != nil {
		return nil, err
	}

	err = engine.Cache.AddRepo(&repo)
	if err != nil {
		return nil, err
	}

	repoimpl := engineImplRepo{
		Repo:          repo,
		MergeRequests: make(map[uint]*model.MergeReq),
		Source:        source,
	}

	engine.Repos = append(engine.Repos, repoimpl)

	return &engine.Repos[len(engine.Repos)-1], nil
}

func (job *engineAddRepoJob) Run(engine *engineImpl) {
	glog.Infof("Add repo job %s", job.Repo.String())
	repoimpl, err := engine.addRepo(job.Repo)
	if err != nil {
		engine.Listener.Status(fmt.Sprintf("Unable to add repo %s: %s",
			job.Repo.String(), err))
		return
	}

	engine.loadMergeRequestCache(repoimpl)
	engine.refreshMergeRequests(repoimpl)

	engine.Listener.RepoAdded(job.Repo)

	engine.saveRepos()
}

func (job *engineRemoveRepoJob) Run(engine *engineImpl) {
	glog.Infof("Remove repo job %s", job.Repo.String())

	for idx, impl := range engine.Repos {
		if impl.Repo.Equal(&job.Repo) {
			engine.Repos = append(engine.Repos[0:idx], engine.Repos[idx+1:]...)
			break
		}
	}

	engine.Listener.RepoRemoved(job.Repo)
}

func (job *engineRefreshReposJob) Run(engine *engineImpl) {
	glog.Info("Refresh repos job")

	engine.RefreshJob.Stop()
	engine.refreshJob()
	engine.RefreshJob.Reset(engine.RefreshInterval)
}

func (engine *engineImpl) getEngineRepo(repo model.Repo) *engineImplRepo {
	for idx, _ := range engine.Repos {
		impl := &engine.Repos[idx]
		if impl.Repo.Equal(&repo) {
			return impl
		}
	}

	return nil
}

func (job *engineLoadMergeRequestCommitDiffsJob) Run(engine *engineImpl) {
	glog.Info("Load commit diffs job")

	impl := engine.getEngineRepo(job.MergeReq.Repo)
	if impl == nil {
		return
	}

	engine.Listener.Status(fmt.Sprintf("Loading %s diff %s",
		job.MergeReq.String(), job.Commit.Hash))
	diffs, err := impl.Source.GetCommitDiffs(&job.MergeReq, &job.Commit)
	if err != nil {
		engine.Listener.Status(fmt.Sprintf("Failed to load commit diffs: %s", err))
	} else {
		newmreq := *impl.MergeRequests[job.MergeReq.ID]
		for sidx, _ := range newmreq.Versions {
			series := &newmreq.Versions[sidx]
			for cidx, _ := range series.Patches {
				commit := &series.Patches[cidx]
				if commit.Hash == job.Commit.Hash {
					commit.Diffs = diffs
					commit.Metadata.Partial = false
				}
			}
		}

		impl.MergeRequests[newmreq.ID] = &newmreq
		err := engine.Cache.SaveMergeRequest(&newmreq)
		if err != nil {
			engine.Listener.Status(fmt.Sprintf("Cannot save merge request %d to cache: %s",
				newmreq.ID, err))
		}
		glog.Infof("Loaded diff %s diff %s", newmreq.String(), job.Commit.Hash)
		engine.Listener.MergeRequestNotify(&newmreq)
		engine.Listener.Status("")
	}
}

func (job *engineRefreshMergeRequestJob) Run(engine *engineImpl) {
	glog.Infof("Refresh merge req job %s", job.MergeReq.String())
	impl := engine.getEngineRepo(job.MergeReq.Repo)
	if impl == nil {
		return
	}

	engine.Listener.Status(fmt.Sprintf("Fetching merge request %d from source",
		job.MergeReq.ID))
	engine.refreshMergeRequest(impl, &job.MergeReq)
	impl.MergeRequests[job.MergeReq.ID] = &job.MergeReq
	engine.Listener.Status("")
	engine.Listener.MergeRequestNotify(&job.MergeReq)
}

func (job *engineAddMergeRequestCommentJob) Run(engine *engineImpl) {
	glog.Infof("Add merge request comment job %s", job.MergeReq.String())

	impl := engine.getEngineRepo(job.MergeReq.Repo)
	if impl == nil {
		return
	}

	engine.Listener.Status(fmt.Sprintf("Posting comment to %s", job.MergeReq.String()))
	err := impl.Source.AddMergeRequestComment(&job.MergeReq, job.Text)
	if err != nil {
		engine.Listener.Status(fmt.Sprintf("Failed to post comment: %s", err))
	} else {
		engine.RefreshMergeRequest(job.MergeReq)
	}
}

func (job *engineAddMergeRequestThreadJob) Run(engine *engineImpl) {
	glog.Infof("Add merge req thread job %s", job.MergeReq.String())

	impl := engine.getEngineRepo(job.MergeReq.Repo)
	if impl == nil {
		return
	}

	engine.Listener.Status(fmt.Sprintf("Posting thread to %s", job.MergeReq.String()))
	err := impl.Source.AddMergeRequestThread(&job.MergeReq, job.Text, job.Context)
	if err != nil {
		engine.Listener.Status(fmt.Sprintf("Failed to post thread: %s", err))
	} else {
		engine.RefreshMergeRequest(job.MergeReq)
	}
}

func (job *engineAddMergeRequestReplyJob) Run(engine *engineImpl) {
	glog.Infof("Add merge req reply job %s %s", job.MergeReq.String(), job.Thread)

	impl := engine.getEngineRepo(job.MergeReq.Repo)
	if impl == nil {
		return
	}

	engine.Listener.Status(fmt.Sprintf("Posting reply to %s thread %s",
		job.MergeReq.String(), job.Thread))
	err := impl.Source.AddMergeRequestReply(&job.MergeReq, job.Thread, job.Text)
	if err != nil {
		engine.Listener.Status(fmt.Sprintf("Failed to post reply: %s", err))
	} else {
		engine.RefreshMergeRequest(job.MergeReq)
	}
}

func (job *engineMergeRequestMarkReadJob) Run(engine *engineImpl) {
	glog.Infof("Mark read job %s", job.MergeReq.String())

	impl := engine.getEngineRepo(job.MergeReq.Repo)
	if impl == nil {
		return
	}

	newmreq := *impl.MergeRequests[job.MergeReq.ID]
	newmreq.Metadata.Status = model.STATUS_READ
	impl.MergeRequests[job.MergeReq.ID] = &newmreq
	err := engine.Cache.SaveMergeRequest(&newmreq)
	if err != nil {
		engine.Listener.Status(fmt.Sprintf("Cannot save merge request %d to cache: %s",
			newmreq.ID, err))
	}
	glog.Infof("Mark read %s", newmreq.String())
	engine.Listener.MergeRequestNotify(&newmreq)
}

func (job *engineMergeRequestAcceptJob) Run(engine *engineImpl) {
	glog.Infof("Merge request accept job %s", job.MergeReq.String())

	impl := engine.getEngineRepo(job.MergeReq.Repo)
	if impl == nil {
		return
	}

	err := impl.Source.AcceptMergeRequest(&job.MergeReq, true)
	if err != nil {
		engine.Listener.Status(fmt.Sprintf("Cannot accept merge request: %s", err))
	} else {
		engine.Listener.Status(fmt.Sprintf("Accepted merge request %s", job.MergeReq.String()))
		engine.RefreshMergeRequest(job.MergeReq)
	}
}

func (job *engineMergeRequestApproveJob) Run(engine *engineImpl) {
	glog.Infof("Merge request approve job %s", job.MergeReq.String())

	impl := engine.getEngineRepo(job.MergeReq.Repo)
	if impl == nil {
		return
	}

	err := impl.Source.ApproveMergeRequest(&job.MergeReq)
	if err != nil {
		engine.Listener.Status(fmt.Sprintf("Cannot approve merge request: %s", err))
	} else {
		engine.Listener.Status(fmt.Sprintf("Approved merge request %s", job.MergeReq.String()))
		engine.RefreshMergeRequest(job.MergeReq)
	}
}

func (job *engineMergeRequestUnapproveJob) Run(engine *engineImpl) {
	glog.Infof("Merge request unapprove job %s", job.MergeReq.String())

	impl := engine.getEngineRepo(job.MergeReq.Repo)
	if impl == nil {
		return
	}

	err := impl.Source.UnapproveMergeRequest(&job.MergeReq)
	if err != nil {
		engine.Listener.Status(fmt.Sprintf("Cannot unapprove merge request: %s", err))
	} else {
		engine.Listener.Status(fmt.Sprintf("Unapproved merge request %s", job.MergeReq.String()))
		engine.RefreshMergeRequest(job.MergeReq)
	}
}

func (engine *engineImpl) updateLoop() {
	glog.Infof("Update loop running")
	for idx, _ := range engine.Repos {
		repo := &engine.Repos[idx]
		glog.Infof("Loading cache for repo %s", repo.Repo.String())
		engine.loadMergeRequestCache(repo)
	}

	for {
		glog.Info("Selecting job")
		select {
		case <-engine.RefreshJob.C:
			glog.Info("Refresh job")
			engine.refreshJob()
			engine.RefreshJob.Reset(engine.RefreshInterval)

		case job := <-engine.Jobs:
			job.Run(engine)
		}

		glog.Info("Next loop iteration")
	}
}

func (engine *engineImpl) AddRepository(repo model.Repo) {
	glog.Infof("Queue add repo %s", repo.String())
	engine.Jobs <- &engineAddRepoJob{
		Repo: repo,
	}
}

func (engine *engineImpl) RemoveRepository(repo model.Repo) {
	glog.Infof("Queue remove repo %s", repo.String())
	engine.Jobs <- &engineRemoveRepoJob{
		Repo: repo,
	}
}

func (engine *engineImpl) RefreshRepos() {
	glog.Info("Queue refresh repos")
	engine.Jobs <- &engineRefreshReposJob{}
}

func (engine *engineImpl) RefreshMergeRequest(mreq model.MergeReq) {
	glog.Infof("Queue refresh merge request %s", mreq.String())
	engine.Jobs <- &engineRefreshMergeRequestJob{
		MergeReq: mreq,
	}
}

func (engine *engineImpl) LoadMergeRequestCommitDiffs(mreq model.MergeReq, commit model.Commit) {
	glog.Infof("Queue load merge request commit diffs %s diff %s",
		mreq.String(), commit.Hash)
	engine.Jobs <- &engineLoadMergeRequestCommitDiffsJob{
		MergeReq: mreq,
		Commit:   commit,
	}
}
func (engine *engineImpl) AddMergeRequestComment(mreq model.MergeReq, text string) {
	glog.Infof("Queue add merge request comment job %s", mreq.String())
	engine.Jobs <- &engineAddMergeRequestCommentJob{
		MergeReq: mreq,
		Text:     text,
	}
}

func (engine *engineImpl) AddMergeRequestThread(mreq model.MergeReq, text string, context *model.CommentContext) {
	glog.Infof("Queue add merge request thread %s", mreq.String())
	engine.Jobs <- &engineAddMergeRequestThreadJob{
		MergeReq: mreq,
		Text:     text,
		Context:  context,
	}
}

func (engine *engineImpl) AddMergeRequestReply(mreq model.MergeReq, thread, text string) {
	glog.Infof("Queue add merge request reply %s", mreq.String())
	engine.Jobs <- &engineAddMergeRequestReplyJob{
		MergeReq: mreq,
		Thread:   thread,
		Text:     text,
	}
}

func (engine *engineImpl) MarkRead(mreq model.MergeReq) {
	glog.Infof("Queue mark read job %s", mreq.String())
	engine.Jobs <- &engineMergeRequestMarkReadJob{
		MergeReq: mreq,
	}
}

func (engine *engineImpl) AcceptMergeRequest(mreq model.MergeReq) {
	glog.Infof("Queue merge request accept %s", mreq.String())
	engine.Jobs <- &engineMergeRequestAcceptJob{
		MergeReq: mreq,
	}
}

func (engine *engineImpl) ApproveMergeRequest(mreq model.MergeReq) {
	glog.Infof("Queue merge request approve %s", mreq.String())
	engine.Jobs <- &engineMergeRequestApproveJob{
		MergeReq: mreq,
	}
}

func (engine *engineImpl) UnapproveMergeRequest(mreq model.MergeReq) {
	glog.Infof("Queue merge request unapprove %s", mreq.String())
	engine.Jobs <- &engineMergeRequestUnapproveJob{
		MergeReq: mreq,
	}
}

func (engine *engineImpl) Run(listener Listener) {
	engine.Listener = listener
	for _, repo := range engine.Repos {
		engine.Listener.RepoAdded(repo.Repo)
	}
	go engine.updateLoop()
}
