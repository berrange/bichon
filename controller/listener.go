// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package controller

import (
	"gitlab.com/bichon-project/bichon/model"
)

type Listener interface {
	Status(msg string)

	// The merge request must be treated as immutable
	// by the caller and receiver
	MergeRequestNotify(mreq *model.MergeReq)

	RepoAdded(repo model.Repo)
	RepoRemoved(repo model.Repo)
}
