// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package model

import (
	"time"
)

type Commit struct {
	Hash      string    `json:"hash"`
	Title     string    `json:"title"`
	Author    User      `json:"author"`
	Committer User      `json:"committer"`
	CreatedAt time.Time `json:"createdAt"`
	UpdatedAt time.Time `json:"updatedAt"`
	Message   string    `json:"message"`
	Diffs     []Diff    `json:"diffs"`

	Metadata CommitMetadata `json:"bichonMetadata"`
}

type CommitMetadata struct {
	Partial bool `json:"partial"`
}

type Diff struct {
	Content     string `json:"content"`
	NewFile     string `json:"newFile"`
	OldFile     string `json:"oldFile"`
	NewMode     string `json:"newMode"`
	OldMode     string `json:"oldMode"`
	CreatedFile bool   `json:"createdFile"`
	RenamedFile bool   `json:"renamedFile"`
	DeletedFile bool   `json:"deletedFile"`
}

func (commit *Commit) Age() string {
	age := time.Since(commit.CreatedAt)
	return formatDuration(age)
}

func (commit *Commit) Activity() string {
	age := time.Since(commit.UpdatedAt)
	return formatDuration(age)
}
