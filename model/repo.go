// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package model

import (
	"fmt"
	"os"
	"path"
	"strings"

	"github.com/go-ini/ini"
	"github.com/golang/glog"
	"gopkg.in/src-d/go-git.v4/plumbing/transport"
)

type Repo struct {
	Directory string
	Remote    string
	Server    string
	Project   string
}

func (repo *Repo) String() string {
	return repo.Server + "/" + repo.Project
}

func (repo *Repo) Equal(otherrepo *Repo) bool {
	return repo.Server == otherrepo.Server && repo.Project == otherrepo.Project
}

func findGitDir(dir string) (string, error) {
	gitdir := path.Join(dir, ".git")
	_, err := os.Stat(gitdir)
	if err != nil {
		if !os.IsNotExist(err) {
			return "", err
		}
		parent, _ := path.Split(dir)
		return findGitDir(parent)
	}

	return dir, nil
}

func FindGitDir() (string, error) {
	here, err := os.Getwd()
	if err != nil {
		return "", err
	}
	return findGitDir(here)
}

func loadRemote(directory, remote string) (string, string, error) {
	gitcfg := path.Join(directory, ".git", "config")

	cfg, err := ini.Load(gitcfg)
	if err != nil {
		return "", "", err
	}

	remoteStr := fmt.Sprintf("remote \"%s\"", remote)
	remoteURL := cfg.Section(remoteStr).Key("url").String()

	if remoteURL == "" {
		return "", "", fmt.Errorf("No remote '%s' found in %s",
			remote, gitcfg)
	}

	ep, err := transport.NewEndpoint(remoteURL)
	if err != nil {
		return "", "", err
	}

	if ep.Host == "" {
		return "", "", fmt.Errorf("Expected a hostname in git remote '%s'", remoteURL)
	}

	return ep.Host, strings.TrimPrefix(strings.TrimSuffix(ep.Path, ".git"), "/"), nil
}

func NewRepo(directory, remote, server, project string) *Repo {
	repo := &Repo{
		Directory: directory,
		Remote:    remote,
		Server:    server,
		Project:   project,
	}

	glog.Infof("Repo dir '%s' remote '%s' host '%s' project '%s'",
		repo.Directory, repo.Remote, repo.Server, repo.Project)

	return repo
}

func NewRepoForDirectory(directory, remote string) (*Repo, error) {
	server, project, err := loadRemote(directory, remote)
	if err != nil {
		return nil, err
	}

	return NewRepo(directory, remote, server, project), nil
}
