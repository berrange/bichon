// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package model

type Account struct {
	UserName string `json:"username"`
	RealName string `json:"realname"`
}
