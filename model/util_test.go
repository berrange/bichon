// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package model

import (
	"testing"
	"time"
)

type DurationInfo struct {
	Native    time.Duration
	Formatted string
}

func TestFormatDuration(t *testing.T) {
	data := []DurationInfo{
		{time.Hour * 24 * 1000, "3 years"},
		{time.Hour * 24 * 366, "1 year"},
		{time.Hour * 24 * 50, "2 mons"},
		{time.Hour * 24 * 25, "4 weeks"},
		{time.Hour * 24 * 25, "4 weeks"},
		{time.Hour * 24 * 11, "2 weeks"},
		{time.Hour * 24 * 6, "6 days"},
		{time.Hour * 50, "2 days"},
		{time.Hour * 24, "1 day"},
		{time.Minute * 600, "10 hours"},
		{time.Minute * 70, "1 hour"},
		{time.Minute * 3, "3 mins"},
		{time.Second * 61, "1 min"},
		{time.Second * 60, "just now"},
		{time.Minute * 1, "just now"},
	}

	for _, entry := range data {
		actual := formatDuration(entry.Native)

		if actual != entry.Formatted {
			t.Errorf("Expected '%s' got '%s'", entry.Formatted, actual)
		}
	}
}
