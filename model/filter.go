// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package model

import (
	"regexp"
	"time"
)

type MergeReqFilter func(mreq MergeReq) bool

func MergeReqFilterReject(this MergeReqFilter) MergeReqFilter {
	return func(mreq MergeReq) bool {
		return !this(mreq)
	}
}

func MergeReqFilterBoth(this, that MergeReqFilter) MergeReqFilter {
	return func(mreq MergeReq) bool {
		return this(mreq) && that(mreq)
	}
}

func MergeReqFilterEither(this, that MergeReqFilter) MergeReqFilter {
	return func(mreq MergeReq) bool {
		return this(mreq) || that(mreq)
	}
}

func MergeReqFilterProject(project string) MergeReqFilter {
	return func(mreq MergeReq) bool {
		return mreq.Repo.Project == project
	}
}

func MergeReqFilterState(state MergeReqState) MergeReqFilter {
	return func(mreq MergeReq) bool {
		return mreq.State == state
	}
}

func MergeReqFilterMetadataStatus(status MergeReqStatus) MergeReqFilter {
	return func(mreq MergeReq) bool {
		return mreq.Metadata.Status == status
	}
}

func MergeReqFilterAge(dur time.Duration) MergeReqFilter {
	then := time.Now().Add(dur * -1)
	return func(mreq MergeReq) bool {
		return mreq.CreatedAt.After(then)
	}
}

func MergeReqFilterActivity(dur time.Duration) MergeReqFilter {
	then := time.Now().Add(dur * -1)
	return func(mreq MergeReq) bool {
		return mreq.UpdatedAt.After(then)
	}
}

func MergeReqFilterSubmitterRealName(namere string) MergeReqFilter {
	re, _ := regexp.Compile(namere)
	return func(mreq MergeReq) bool {
		if re != nil {
			return re.Match([]byte(mreq.Submitter.RealName))
		} else {
			return true
		}
	}
}

func MergeReqFilterSubmitterUserName(namere string) MergeReqFilter {
	re, _ := regexp.Compile(namere)
	return func(mreq MergeReq) bool {
		if re != nil {
			return re.Match([]byte(mreq.Submitter.RealName))
		} else {
			return true
		}
	}
}
