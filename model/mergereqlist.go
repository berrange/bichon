// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package model

import (
	"github.com/golang/glog"
)

type MergeReqList struct {
	Master []*MergeReq
	Active []*MergeReq
	Filter MergeReqFilter
	Sorter MergeReqSorter
}

func (mreqs *MergeReqList) Insert(mreq *MergeReq) {
	found := false
	for idx, oldmreq := range mreqs.Master {
		if oldmreq.Equal(mreq) {
			mreqs.Master[idx] = mreq
			found = true
			glog.Info("Updated existing mreq")
			break
		}
	}
	if !found {
		glog.Info("Got new mreq")
		mreqs.Master = append(mreqs.Master, mreq)
	}

	mreqs.ReFilter()
}

func (mreqs *MergeReqList) ReSort() {
	mreqs.Active = MergeReqSort(mreqs.Active, mreqs.Sorter)
}

func (mreqs *MergeReqList) ReFilter() {
	if mreqs.Filter == nil {
		mreqs.Active = MergeReqSort(mreqs.Master, mreqs.Sorter)
	} else {
		active := make([]*MergeReq, 0)

		for _, mreq := range mreqs.Master {
			if mreqs.Filter(*mreq) {
				active = append(active, mreq)
			}
		}

		mreqs.Active = MergeReqSort(active, mreqs.Sorter)
	}
}
